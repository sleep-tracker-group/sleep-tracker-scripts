from analyzeData import analyzeData_percentile, analyzeData_linreg
from helper import helper_comparison, helper_analyze_main

analysis = [
    {"name": "Percentile", "func": analyzeData_percentile.analyze_data, "parameters": {"min_deep_sleep_length": 15}},
    {"name": "LinReg", "func": analyzeData_linreg.analyze_data,
     "parameters": {"min_deep_sleep_length": 15.0, "steps": 50, "angle_threshold": 0.001}}
]

options = "SELECT ANALYSIS FOR ALL NIGHTS\n"
for i in range(len(analysis)):
    options += "[" + str(i) + "] - " + analysis[i]["name"] + "\n"

print(options)
input_var = int(input("Enter option: "))
print("You selected: " + analysis[input_var]["name"])
print("##################################")

parameters = {}
if analysis[input_var]["parameters"] is not None:
    print("DEFAULT PARAMETERS\n")
    for key in analysis[input_var]["parameters"].keys():
        print(key + ": " + str(analysis[input_var]["parameters"][key]))
    print("##################################")

    if input("Do you want to use custom parameters?\n[y] - Yes\nother char - No\nEnter option: ") == "y":
        print("##################################\nSET PARAMETERS\n")
        for key in analysis[input_var]["parameters"].keys():
            analysis[input_var]["parameters"][key] = float(input(key + ": "))

    print("##################################")

    parameters = analysis[input_var]["parameters"]
print("RUN ANALYZE_DATA")
helper_analyze_main.analyze_all_nights(analysis[input_var]["func"], parameters)

print("##################################")
print("RUN COMPARE_EVALUATIONS")
helper_comparison.compare_evaluations()