from variants import v1, v2, v3, v4, helper
import os
import matplotlib.dates as dates
import matplotlib.pyplot as plt

analysis = [
    {"name": "V1", "func": v1.analyze_data, "needs_data": { "gyroscope": True, "accelerometer": False }, "parameters": {"seconds_to_combine": 300}},
    {"name": "V2", "func": v2.analyze_data, "needs_data": { "gyroscope": True, "accelerometer": True }, "parameters": {"seconds_to_combine": 300}},
    {"name": "V3", "func": v3.analyze_data, "needs_data": { "gyroscope": True, "accelerometer": False }, "parameters": {"seconds_to_combine": 5, "floating_avg_padding": 300, "trend_step": 20}},
    {"name": "V4", "func": v4.analyze_data, "needs_data": { "gyroscope": True, "accelerometer": False }, "parameters": {"padding": 30}}
]

#############################################################################################
#############################################################################################
# Select Variant
#############################################################################################
#############################################################################################

options = "SELECT VARIANT FOR A NIGHT\n"
for i in range(len(analysis)):
    options += "[" + str(i) + "] - " + analysis[i]["name"] + "\n"

print(options)
variant = int(input("Enter option: "))
print("You selected: " + analysis[variant]["name"])
print("##################################")

#############################################################################################
#############################################################################################
# Set Parameters
#############################################################################################
#############################################################################################
parameters = {}
if analysis[variant]["parameters"] is not None:
    print("DEFAULT PARAMETERS\n")
    for key in analysis[variant]["parameters"].keys():
        print(key + ": " + str(analysis[variant]["parameters"][key]))
    print("##################################")

    if input("Do you want to use custom parameters?\n[y] - Yes\nother char - No\nEnter option: ") == "y":
        print("##################################\nSET PARAMETERS\n")
        for key in analysis[variant]["parameters"].keys():
            analysis[variant]["parameters"][key] = float(input(key + ": "))

    print("##################################")

    parameters = analysis[variant]["parameters"]

#############################################################################################
#############################################################################################
# Select Night
#############################################################################################
#############################################################################################
folder = "./../nights"
nights = [path for path in os.listdir(folder) if os.path.isdir(folder+"/"+path) and path[0] != "." and path[0] != "_" ]
nights.sort()

options = "SELECT NIGHT\n"
for i in range(len(nights)):
    options += "[" + str(i) + "] - " + nights[i] + "\n"

print(options)
night = int(input("Enter option: "))
print("You selected: " + nights[night])


#############################################################################################
#############################################################################################
# Load Data
#############################################################################################
#############################################################################################
selected_night_folder = folder + "/" + nights[i]

path_gyroscope =  selected_night_folder + "/csv/gyroscope.csv"
path_accelerometer =  selected_night_folder + "/csv/accelerometer.csv"

data_gyroscope = {}
data_accelerometer = {}

if analysis[variant]["needs_data"]["gyroscope"] and os.path.isfile(path_gyroscope):
    data_gyroscope = helper.import_csv(path_gyroscope)
    data_gyroscope["timestamp"] = helper.convert_timestamp(data_gyroscope["timestamp"])

if analysis[variant]["needs_data"]["accelerometer"] and os.path.isfile(path_accelerometer):
    data_accelerometer = helper.import_csv(path_accelerometer)
    data_accelerometer["timestamp"] = helper.convert_timestamp(data_accelerometer["timestamp"])



#############################################################################################
#############################################################################################
# Run analyze
#############################################################################################
#############################################################################################
date_form = dates.DateFormatter("%H:%M:%S")

if analysis[variant]["needs_data"]["gyroscope"] and not analysis[variant]["needs_data"]["accelerometer"]:
    analysis[variant]["func"](data_gyroscope, parameters, date_form)
elif analysis[variant]["needs_data"]["gyroscope"] and analysis[variant]["needs_data"]["accelerometer"]:
    analysis[variant]["func"](data_accelerometer, data_gyroscope, parameters, date_form)

plt.show()