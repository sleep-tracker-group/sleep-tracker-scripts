from . import helper
import matplotlib.pyplot as plt
import numpy as np
import math


def analyze_data(raw_gyro_data, parameters, date_form):
    """
    Analyze the given gyroscope data and try to classify using percentages of different sleep phases.
    Plot the classified data.
    """
    data = aggregate_data(raw_gyro_data, parameters["seconds_to_combine"])
    vector_data = helper.data_to_vectorlen(data)

    fig_title = "Gyro vector lengths with aggregated points"
    plt.figure(fig_title)
    plt.plot(vector_data["timestamp"], vector_data["vectorlen"])
    ax = plt.gca()
    plt.title(fig_title)
    ax.xaxis.set_major_formatter(date_form)
    plt.gcf().autofmt_xdate()

    sorted_vector_data = np.sort(np.array(vector_data["vectorlen"]))

    percentage_deep_sleep = 0.17
    percentage_light_and_rem = 0.51 + 0.23
    percentage_awake = 0.09

    threshold_deep_sleep = sorted_vector_data[math.ceil(percentage_deep_sleep * len(sorted_vector_data))]
    threshold_light_and_rem = sorted_vector_data[
        math.ceil((percentage_deep_sleep + percentage_light_and_rem) * len(sorted_vector_data))]

    fig_title = "Sorted vector data, classified using percentages"
    plt.figure(fig_title)
    plt.plot(sorted_vector_data, 'bo--', label='Awake')
    plt.plot(sorted_vector_data[sorted_vector_data < threshold_light_and_rem], 'go--', label='Light and REM sleep')
    plt.plot(sorted_vector_data[sorted_vector_data < threshold_deep_sleep], 'ro--', label='Deep sleep')
    plt.legend(loc="upper left")
    plt.title(fig_title)

    sleep_phase = vector_data["vectorlen"].copy()
    sleep_phase[0] = 2
    sleep_phase[len(sleep_phase) - 1] = 2
    for i in range(1, len(sleep_phase) - 1):
        if vector_data["vectorlen"][i] <= threshold_deep_sleep:
            sleep_phase[i] = 0
        elif vector_data["vectorlen"][i] <= threshold_light_and_rem:
            sleep_phase[i] = 1
        else:
            sleep_phase[i] = 2

    fig_title = "Sleep phases"
    plt.figure(fig_title)
    plt.plot(data["timestamp"], sleep_phase)
    ax = plt.gca()
    ax.set_yticks([0, 1, 2])
    ax.set_yticklabels(["Deep sleep", "Light and REM sleep", "Awake"])
    plt.title(fig_title)
    ax.xaxis.set_major_formatter(date_form)
    plt.gcf().autofmt_xdate()


def aggregate_data(data, seconds):
    combined_data = {}
    for key in data.keys():
        combined_data[key] = []
    for i in range(len(data["x"])):
        combined_pos = math.floor(i / (2 * seconds))
        for key in data.keys():
            if len(combined_data[key]) == combined_pos:
                combined_data[key].append(0)
            if key == "timestamp":
                combined_data[key][combined_pos] = data[key][i]
            else:
                value = abs(data[key][i])
                if value > combined_data[key][combined_pos]:
                    combined_data[key][combined_pos] = value
    return combined_data