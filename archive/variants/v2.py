from . import helper
import matplotlib.pyplot as plt
import math
import numpy as np


def analyze_data(raw_acc_data, raw_gyro_data, parameters, date_form):
    """
    Analyze the given gyroscope and accelerometer data and try to classify using percentages of different sleep phases.
    Plot the classified data.
    """
    acc_data = aggregate_data(raw_acc_data, parameters["seconds_to_combine"])
    gyro_data = aggregate_data(raw_gyro_data, parameters["seconds_to_combine"])

    acc_vector_data = helper.data_to_vectorlen(acc_data)
    gyro_vector_data = helper.data_to_vectorlen(gyro_data)

    fig_title = "Gyro vector lengths with aggregated points"
    plt.figure(fig_title)
    plt.plot(gyro_vector_data["timestamp"], gyro_vector_data["vectorlen"])
    ax = plt.gca()
    plt.title(fig_title)
    ax.xaxis.set_major_formatter(date_form)
    plt.gcf().autofmt_xdate()

    fig_title = "Acc vector lengths with aggregated points"
    plt.figure(fig_title)
    plt.plot(acc_vector_data["timestamp"], acc_vector_data["vectorlen"])
    ax = plt.gca()
    plt.title(fig_title)
    ax.xaxis.set_major_formatter(date_form)
    plt.gcf().autofmt_xdate()

    acc_sorted_vector = np.sort(np.array(acc_vector_data["vectorlen"]))
    gyro_sorted_vector = np.sort(np.array(gyro_vector_data["vectorlen"]))

    gyro_min = gyro_sorted_vector[0]
    gyro_max = gyro_sorted_vector[len(gyro_sorted_vector) - 1]
    for i in range(len(gyro_vector_data["vectorlen"])):
        gyro_vector_data["vectorlen"][i] = (gyro_vector_data["vectorlen"][i] - gyro_min) / (gyro_max - gyro_min)

    fig_title = "Gyro vector lengths with aggregated points, modified value range"
    plt.figure(fig_title)
    plt.plot(gyro_vector_data["timestamp"], gyro_vector_data["vectorlen"])
    ax = plt.gca()
    plt.title(fig_title)
    ax.xaxis.set_major_formatter(date_form)
    plt.gcf().autofmt_xdate()

    acc_min = acc_sorted_vector[0]
    acc_max = acc_sorted_vector[len(acc_sorted_vector) - 1]
    for i in range(len(acc_vector_data["vectorlen"])):
        acc_vector_data["vectorlen"][i] = (acc_vector_data["vectorlen"][i] - acc_min) / (acc_max - acc_min)

    combined = []
    for i in range(len(gyro_vector_data["vectorlen"])):
        combined.append(gyro_vector_data["vectorlen"][i] + acc_vector_data["vectorlen"][i])

    fig_title = "Combined vector lengths"
    plt.figure(fig_title)
    plt.plot(acc_data["timestamp"], combined)
    ax = plt.gca()
    plt.title(fig_title)
    ax.xaxis.set_major_formatter(date_form)
    plt.gcf().autofmt_xdate()

    data = {"timestamp": acc_data["timestamp"].copy(), "value": combined.copy()}

    sorted_vector_data = np.sort(np.array(combined))

    percentage_deep_sleep = 0.17
    percentage_light_and_rem = 0.51 + 0.23
    percentage_awake = 0.09

    threshold_deep_sleep = sorted_vector_data[math.ceil(percentage_deep_sleep * len(sorted_vector_data))]
    threshold_light = sorted_vector_data[
        math.ceil((percentage_deep_sleep + percentage_light_and_rem) * len(sorted_vector_data))]

    fig_title = "Combined & sorted vector data, classified using percentages"
    plt.figure(fig_title)
    plt.plot(sorted_vector_data, 'bo--', label="Awake")
    plt.plot(sorted_vector_data[sorted_vector_data < threshold_light], 'go--', label="Light and REM sleep")
    plt.plot(sorted_vector_data[sorted_vector_data < threshold_deep_sleep], 'ro--', label="deep sleep")
    plt.legend()
    plt.title(fig_title)

    for i in range(len(data["value"])):
        if data["value"][i] <= threshold_deep_sleep:
            data["value"][i] = 0
        elif data["value"][i] <= threshold_light:
            data["value"][i] = 1
        else:
            data["value"][i] = 2

    fig_title = "Sleep phases"
    plt.figure(fig_title)
    plt.plot(data["timestamp"], data["value"])
    ax = plt.gca()
    ax.set_yticks([0, 1, 2])
    ax.set_yticklabels(["Deep sleep", "Light and REM sleep", "Awake"])
    plt.title(fig_title)
    ax.xaxis.set_major_formatter(date_form)
    plt.gcf().autofmt_xdate()


def aggregate_data(data, seconds):
    combined_data = {}
    for key in data.keys():
        combined_data[key] = []
    for i in range(len(data["x"])):
        combined_pos = math.floor(i / (2 * seconds))
        for key in data.keys():
            if len(combined_data[key]) == combined_pos:
                combined_data[key].append(0)
            if key == "timestamp":
                combined_data[key][combined_pos] = data[key][i]
            else:
                value = abs(data[key][i])
                if value > combined_data[key][combined_pos]:
                    combined_data[key][combined_pos] = value
    return combined_data