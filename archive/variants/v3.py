from . import helper
import matplotlib.pyplot as plt
import math


def analyze_data(raw_data, parameters, date_form):
    """
    Aggregate the given gyroscope data with the floating average, analyze it and calculate local minimum/maximum points.
    Plot floating averages and min/max points.
    """
    data = aggregate_data(raw_data, parameters["seconds_to_combine"])
    vector_data = helper.data_to_vectorlen(data)

    fig_title = "Gyro vector lengths with aggregated points"
    plt.figure(fig_title)
    plt.plot(vector_data["timestamp"], vector_data["vectorlen"])
    ax = plt.gca()
    plt.title(fig_title)
    ax.xaxis.set_major_formatter(date_form)
    plt.gcf().autofmt_xdate()

    vector_data = floating_avg(vector_data, int(parameters["floating_avg_padding"]))

    fig_title = "Floating average of gyroscope data"
    plt.figure(fig_title)
    plt.plot(vector_data["timestamp"], vector_data["vectorlen"])
    ax = plt.gca()
    plt.title(fig_title)
    ax.xaxis.set_major_formatter(date_form)
    plt.gcf().autofmt_xdate()

    trend_data = {"timestamp": [], "vectorlen": []}
    current_trend = helper.get_trend(vector_data["vectorlen"][0], vector_data["vectorlen"][1])
    for i in range(parameters["trend_step"], len(vector_data["vectorlen"]), parameters["trend_step"]):
        new_trend = helper.get_trend(vector_data["vectorlen"][i - parameters["trend_step"]], vector_data["vectorlen"][i])
        if new_trend < current_trend or new_trend > current_trend:
            trend_data["timestamp"].append(vector_data["timestamp"][i])
            trend_data["vectorlen"].append(vector_data["vectorlen"][i])

    fig_title = "Sleep trend minimum / maximum"
    plt.figure(fig_title)
    plt.plot(trend_data["timestamp"], trend_data["vectorlen"], 'o--')
    ax = plt.gca()
    plt.title(fig_title)
    ax.xaxis.set_major_formatter(date_form)
    plt.gcf().autofmt_xdate()


def aggregate_data(data, seconds):
    combined_data = {}
    for key in data.keys():
        combined_data[key] = []
    for i in range(len(data["x"])):
        combined_pos = math.floor(i / (2 * seconds))
        for key in data.keys():
            if len(combined_data[key]) == combined_pos:
                combined_data[key].append(0)
            if key == "timestamp":
                combined_data[key][combined_pos] = data[key][i]
            else:
                value = abs(data[key][i])
                if value > combined_data[key][combined_pos]:
                    combined_data[key][combined_pos] = value
    return combined_data


def floating_avg(data, padding):
    combined_data = {"timestamp": [], "vectorlen": []}
    for i in range(padding, len(data["vectorlen"]) - padding):
        val = sum(data["vectorlen"][i - padding: i + padding]) / len(data["vectorlen"][i - padding: i + padding])
        combined_data["vectorlen"].append(val)
        combined_data["timestamp"].append(data["timestamp"][i])
    return combined_data