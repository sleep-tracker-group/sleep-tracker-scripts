import csv
from datetime import datetime


def import_csv(filename):
    with open(filename) as file:
        csv_reader = csv.reader(file, delimiter=';')
        list_data = list(csv_reader)
        list_dict = {}
        for i, header in enumerate(list_data[0]):
            header = header.strip()
            item_list = []
            for j, item in enumerate(list_data):
                if j > 0:
                    if header == "timestamp" or header == "sleepID":
                        number = int(item[i])
                    else:
                        number = float(item[i])
                    item_list.append(number)
            list_dict[header] = item_list
        return list_dict


def data_to_vectorlen(data):
    vector_data = []
    for i in range(len(data["x"])):
        vector_data.append(calc_vec_length(data["x"][i], data["y"][i], data["z"][i]))
    result = {"timestamp": data["timestamp"].copy(), "vectorlen": vector_data}
    return result


def convert_timestamp(old_timestamps):
    new_time = []
    for time in old_timestamps:
        new_time.append(datetime.fromtimestamp(time / 1000))
    return new_time


def calc_vec_length_2(a, b):
    return pow((pow(a, 2) + pow(b, 2)), 0.5)


def calc_vec_length(a, b, c):
    return pow((pow(a, 2) + pow(b, 2) + pow(c, 2)), 0.5)


def get_trend(before, now):
    result = 0
    if before > now:
        result = -1
    elif before < now:
        result = 1
    return result
