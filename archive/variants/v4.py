import matplotlib.pyplot as plt
import math
import numpy as np
from . import helper


def analyze_data(raw_gyro_data, parameters, date_form):
    """
    Analyze the given gyroscope data and try to apply the discrete curve evolution algorithm.
    Plot different steps of the algorithm.
    """
    vectorlen_data = helper.data_to_vectorlen(raw_gyro_data)

    floating_sum_data = vectorlen_floating_sum(vectorlen_data, parameters["padding"])

    floating_sum_avg = sum(floating_sum_data["vectorlen"]) / len(floating_sum_data["vectorlen"])

    fig_title = "floating sum over time (padding " + str(parameters["padding"]) + " values)"
    plt.figure(fig_title)
    plt.plot(floating_sum_data["timestamp"], floating_sum_data["vectorlen"], 'b')
    plt.plot(vectorlen_data["timestamp"], np.array(vectorlen_data["vectorlen"]) + floating_sum_avg, 'r')
    ax = plt.gca()
    plt.title(fig_title)
    ax.xaxis.set_major_formatter(date_form)
    plt.gcf().autofmt_xdate()

    vector_array = vectors_inbetween(floating_sum_data)

    array_sum = get_vector_array_sum(vector_array)

    for i in range(len(vector_array) - 1):
        vector_array[i]["k"] = calc_k_value(vector_array[i]["xDiff"],
                                            vector_array[i]["yDiff"],
                                            vector_array[i + 1]["xDiff"],
                                            vector_array[i + 1]["yDiff"],
                                            array_sum)

    plot_dict = vector_array_to_dict(vector_array)

    fig_title = "K values over time"
    plt.figure(fig_title)
    plt.plot(plot_dict["end"], plot_dict["k"])
    ax = plt.gca()
    plt.title(fig_title)
    ax.xaxis.set_major_formatter(date_form)
    plt.gcf().autofmt_xdate()

    fig_title = "vector lengths over time"
    plt.figure(fig_title)
    plt.plot(vectorlen_data["timestamp"], vectorlen_data["vectorlen"])
    ax = plt.gca()
    plt.title(fig_title)
    ax.xaxis.set_major_formatter(date_form)
    plt.gcf().autofmt_xdate()

    filtered_data = filter_k_array(vectorlen_data, vector_array, 0.09)

    fig_title = "filtered vector lengths over time"
    plt.figure(fig_title)
    plt.plot(filtered_data["timestamp"], filtered_data["vectorlen"])
    ax = plt.gca()
    plt.title(fig_title)
    ax.xaxis.set_major_formatter(date_form)
    plt.gcf().autofmt_xdate()


def vectors_inbetween(data):
    vector_array = []
    for i in range(len(data["vectorlen"]) - 1):
        vector_array.append({"xDiff": (data["timestamp"][i + 1] - data["timestamp"][i]).microseconds / 1000,
                             "yDiff": data["vectorlen"][i + 1] - data["vectorlen"][i],
                             "start": data["timestamp"][i],
                             "end": data["timestamp"][i + 1]})
    return vector_array


def get_vector_angle(v1x, v1y, v2x, v2y):
    dot = v1x * v2x + v1y * v2y
    v1len = helper.calc_vec_length_2(v1x, v1y)
    v2len = helper.calc_vec_length_2(v2x, v2y)
    return math.acos(dot / (v1len * v2len)) * (math.pi / 180)


def get_vector_array_sum(vector_array):
    result = 0
    for i in range(len(vector_array)):
        result += helper.calc_vec_length_2(vector_array[i]["xDiff"], vector_array[i]["yDiff"])
    return result


def calc_k_value(v1x, v1y, v2x, v2y, array_sum):
    angle = get_vector_angle(v1x, v1y, v2x, v2y)
    v1_ratio = get_len_ratio(v1x, v1y, array_sum)
    v2_ratio = get_len_ratio(v2x, v2y, array_sum)
    return ((angle * v1_ratio * v2_ratio) / (v1_ratio + v2_ratio)) * 10000000


def get_len_ratio(v1x, v1y, vector_sum):
    v_len = helper.calc_vec_length_2(v1x, v1y)
    return v_len / vector_sum


def filter_k_array(original_dict, vector_array, threshold):
    result = {"timestamp": [], "vectorlen": []}
    for i in range(len(vector_array) - 1):
        if vector_array[i]["k"] > threshold:
            result["timestamp"].append(original_dict["timestamp"][i])
            result["vectorlen"].append(original_dict["vectorlen"][i])
    return result


def vector_array_to_dict(vector_array):
    result = {"end": [], "k": []}
    for i in range(len(vector_array) - 1):
        result["end"].append(vector_array[i]["end"])
        result["k"].append(vector_array[i]["k"])
    return result


def vectorlen_floating_sum(vectorlen_data, padding):
    combined_data = {"timestamp": [], "vectorlen": []}
    numpy_array = np.array(vectorlen_data["vectorlen"])
    for i in range(padding, len(vectorlen_data["vectorlen"]) - padding):
        val = sum(numpy_array[i - padding: i + padding])
        combined_data["vectorlen"].append(val)
        combined_data["timestamp"].append(vectorlen_data["timestamp"][i])
    return combined_data
