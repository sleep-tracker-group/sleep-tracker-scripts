import matplotlib.pyplot as plt
import numpy as np
from helper import helper, helper_json

def analyze_data(sleep_data, save_path, date_form, parameters):
    """
    Analyze given sleep data and extracts deep sleep phases with percentile.
    Write deep sleep timestamps to JSON in given path.
    """
    vectorlen_data = helper.set_vectorlen_median(helper.data_to_vectorlen(sleep_data))

    numpy_array = np.array(vectorlen_data["vectorlen"])

    vectorlen_threshold = np.percentile(numpy_array, 98.9)

    helper.plot_sorted_data(vectorlen_data, vectorlen_threshold, "sorted array data, filtered with percentile")

    filtered_array = helper.filter_vectorlen_data(vectorlen_data, vectorlen_threshold, date_form)

    deep_sleep = helper.get_deep_sleep(filtered_array, parameters["min_deep_sleep_length"])

    fig_title = "vector length over time, deep sleep marked"
    plt.figure(fig_title)
    plt.plot(vectorlen_data["timestamp"], vectorlen_data["vectorlen"], 'b', label="original data")
    for i in range(len(deep_sleep["deep_start"])):
        plt.axvspan(deep_sleep["deep_start"][i], deep_sleep["deep_end"][i], color='green', alpha=0.3)
    ax = plt.gca()
    plt.title(fig_title)
    ax.xaxis.set_major_formatter(date_form)
    plt.gcf().autofmt_xdate()

    helper_json.write_evaluation_file(
        helper_json.deep_sleep_to_json(deep_sleep, sleep_data["timestamp"][0],
                                       sleep_data["timestamp"][len(sleep_data["timestamp"]) - 1]), save_path)
