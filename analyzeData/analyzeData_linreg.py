import matplotlib.pyplot as plt
import numpy as np
from helper import helper, helper_json
from scipy import stats

def analyze_data(sleep_data, save_path, date_form, parameters):
    """
    Analyze given sleep data and extracts deep sleep phases with linear regression.
    Write deep sleep timestamps to JSON in given path.
    """
    vectorlen_data = helper.set_vectorlen_median(helper.data_to_vectorlen(sleep_data))

    vectorlen_threshold = linear_regression(vectorlen_data, parameters["angle_threshold"], parameters["steps"],
                                            parameters["min_deep_sleep_length"])

    filtered_array = helper.filter_vectorlen_data(vectorlen_data, vectorlen_threshold, date_form)

    deep_sleep = helper.get_deep_sleep(filtered_array, parameters["min_deep_sleep_length"])

    fig_title = "vector length over time, deep sleep marked"
    plt.figure(fig_title)
    plt.plot(vectorlen_data["timestamp"], vectorlen_data["vectorlen"], 'b', label="original data")
    for i in range(len(deep_sleep["deep_start"])):
        plt.axvspan(deep_sleep["deep_start"][i], deep_sleep["deep_end"][i], color='green', alpha=0.3)
    ax = plt.gca()
    plt.title(fig_title)
    ax.xaxis.set_major_formatter(date_form)
    plt.gcf().autofmt_xdate()

    helper_json.write_evaluation_file(
        helper_json.deep_sleep_to_json(deep_sleep, sleep_data["timestamp"][0],
                                       sleep_data["timestamp"][len(sleep_data["timestamp"]) - 1]),
        save_path)


def linear_regression(vectorlen_data, angle_threshold, steps, minutes):
    """
    Calculate the first significant vector length based on linear regression.
    """
    steps = int(steps)
    print(f"Steps: {steps} threshold: {angle_threshold} minutes: {minutes}")

    sorted_array = np.sort(np.array(vectorlen_data["vectorlen"]))

    first_sig_slope_index = 0
    i = 0
    while i < len(sorted_array):
        if i + steps < len(sorted_array):
            max_index = i + steps
        else:
            max_index = len(sorted_array)
        if len(sorted_array[i:max_index]) > 1:
            slope, intercept, r_value, p_value, std_err = stats.linregress(range(0, max_index - i),
                                                                           sorted_array[i:max_index])
        else:
            slope = 0

        if first_sig_slope_index == 0 and slope > angle_threshold:
            first_sig_slope_index = i
        i += steps

    first_sig_vectorlen = sorted_array[first_sig_slope_index]

    helper.plot_sorted_data(vectorlen_data, first_sig_vectorlen, "sorted array data, filtered with linear "
                                                                 "regression")

    return first_sig_vectorlen
