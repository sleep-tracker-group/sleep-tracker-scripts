# Sleep Tracker - Scripts

This repository was used to try out different analysis for recognising deep sleep
of the raw movement data.  
Therefore a folder structure was created in which all nights are stored by using
the following guideline:

## Nights
In the ```nights```-folder you can find all nights of one proband. Each night
is stored in a subfolder which is named by YYYY_mm_DD-*proband-name*. This
folder contains ```csv```and ```sleep-cycle```.  
```csv``` includes the raw data of the gyroscope (```gyroscope.csv```) and the 
accelerometer (```accelerometer.csv```).  
```sleep-cycle``` contains the screenshot of the app's analysis-result, which 
was rasterized manually to make it easier to read the deep sleep periods.
It is stored because a comparison was needed to evaluate the analysis-variants.  
The deep sleep periods are saved in ```sleep-cycle.json```by using the following
structure:
```
{
    "start": [
         "dd.mm.YYYY hh:mm:ss",
         ...
     ],
    "end": [
         "dd.mm.YYYY hh:mm:ss",
         ...
     ]
 }
```
It is very important that you follow this naming convention if you want to add 
another night!

## Archive
At the beginning results of scientific studies (like deep sleep percentage, 
sleep cycle durations, ...) were used to analyze the collected
raw data. This resulted in analyzes that are suitable for the average person and
are not based on the individual behavior of a test person.  
Execute the ```interactive_archive_runner.py``` for running the old analyzes for
one single night.  
Each variant requires to implement the function 
```analyze_data(raw_gyro_data, parameters, date_form)``` or 
```analyze_data(raw_acc_data, raw_gyro_data, parameters, date_form)``` as the 
main entrypoint. ```parameters``` is an object that contains all the parameters 
of the variant that can be varieted and specified in the interactive-runner.

## Root folder
The analyses, which can be executed by using ```1_interactive_runner.py```, adapt 
to individual behavior. They filter the noise out of the data and are looking for 
time spans where no significant activity was recognised.  
```1_interactive_runner.py``` lets you select an algorithm and runs the selected 
option for all nights. All analyses are creating an ```evaluation.json``` in 
the nights folder:
```
{"tracking_start": "29.06.2020 00:53:47", "tracking_end": "29.06.2020 09:00:30"}
{
    "start": [
         "dd.mm.YYYY hh:mm:ss",
         ...
     ],
    "end": [
        "dd.mm.YYYY hh:mm:ss",
        ...
     ],
    "tracking_start": "dd.mm.YYYY hh:mm:ss",
    "tracking_end": "dd.mm.YYYY hh:mm:ss"
 }
```
Finally the runner creates a ```evaluation.xlsx``` which compares the results of
the analysis with the app Sleep Cycle by using the ```evaluation.json``` and 
```sleep-cycle.json``` of every night.  

```2_interactive_config_comparison_runner.py``` lets you select an algorithm. Additionally you can define ranges for every parameter and the scripts creates every possible combination of those ranges.
The script then runs the algorithm for all parameter sets and creates a ```config_comparison.xlsx``` for you to compare the results.

Each variant requires to implement the function 
```analyze_data(sleep_data, save_path, date_form, parameters)``` as the 
main entrypoint. ```sleep_data``` contains all raw gyroscope-data. 
```save_path``` is the path to the night in which the ```evaluation.json``` has
to be stored. ```parameters``` is an object that contains all the parameters 
of the variant that can be varieted and specified in the interactive-runner.
