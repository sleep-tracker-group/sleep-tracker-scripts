from datetime import datetime
import json


def read(path):
    result = {}
    with open(path, "r") as i:
        result = json.load(i)
    return result


def convert_dates(json_object):
    for i in range(len(json_object["start"])):
        json_object["start"][i] = datetime.strptime(json_object["start"][i], '%d.%m.%Y %H:%M:%S')
    for i in range(len(json_object["end"])):
        json_object["end"][i] = datetime.strptime(json_object["end"][i], '%d.%m.%Y %H:%M:%S')
    
    if "tracking_start" in json_object and "tracking_end" in json_object:
        json_object["tracking_start"] = datetime.strptime(json_object["tracking_start"], '%d.%m.%Y %H:%M:%S')
        json_object["tracking_end"] = datetime.strptime(json_object["tracking_end"], '%d.%m.%Y %H:%M:%S')
    return json_object


def deep_sleep_to_json(deep_sleep_data, tracking_start, tracking_end):
    """
    Convert the given deep sleep data to a JSON object with additional information on tracking.
    """
    result = {
        "start": [],
        "end": [],
        "tracking_start": tracking_start.strftime('%d.%m.%Y %H:%M:%S'),
        "tracking_end": tracking_end.strftime('%d.%m.%Y %H:%M:%S')
    }
    for i in range(len(deep_sleep_data["deep_start"])):
        result["start"].append(deep_sleep_data["deep_start"][i].strftime('%d.%m.%Y %H:%M:%S'))
        result["end"].append(deep_sleep_data["deep_end"][i].strftime('%d.%m.%Y %H:%M:%S'))
    return result


def write_evaluation_file(json_data, path):
    """
    Write given JSON data to path.
    """
    with open(path + "/evaluation.json", "w") as out:
        json.dump(json_data, out)