import matplotlib.dates as dates
import os
from helper import helper


def analyze_all_nights(analyze_data, parameters):
    date_form = dates.DateFormatter("%H:%M:%S")

    for path in os.listdir("./nights"):
        path = "nights/" + path
        if os.path.isdir(path) and path[0] != "." and path[0] != "_":
            print("Folder: " + path)
            file_path = path + "/csv/gyroscope.csv"
            if os.path.isfile(file_path):
                data = helper.import_csv(file_path)
                data["timestamp"] = helper.convert_timestamp(data["timestamp"])
                analyze_data(data, path, date_form, parameters)
                # plt.show()
