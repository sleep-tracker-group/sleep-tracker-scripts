import csv
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.dates as dates
from datetime import datetime, timedelta


def import_csv(filename):
    with open(filename) as file:
        csv_reader = csv.reader(file, delimiter=';')
        list_data = list(csv_reader)
        list_dict = {}
        for i, header in enumerate(list_data[0]):
            header = header.strip()
            item_list = []
            for j, item in enumerate(list_data):
                if j > 0:
                    if header == "timestamp" or header == "sleepID":
                        number = int(item[i])
                    else:
                        number = float(item[i])
                    item_list.append(number)
            list_dict[header] = item_list
        return list_dict


def filter_vectorlen_data(vectorlen_data, vectorlen_threshold, date_form):
    """
    Filter given dict with "timestamp" and "vectorlen", all values below threshold are insignificant and set to zero.
    Plot old and new vector lengths.
    """
    filtered_array = {"timestamp": [], "vectorlen": []}
    for i in range(len(vectorlen_data["vectorlen"])):
        if vectorlen_data["vectorlen"][i] > vectorlen_threshold:
            filtered_array["timestamp"].append(vectorlen_data["timestamp"][i])
            filtered_array["vectorlen"].append(vectorlen_data["vectorlen"][i])
        else:
            filtered_array["timestamp"].append(vectorlen_data["timestamp"][i])
            filtered_array["vectorlen"].append(0)

    fig_title = "vector length over time"
    plt.figure(fig_title)
    plt.plot(vectorlen_data["timestamp"], vectorlen_data["vectorlen"], 'b', label="original data")
    plt.plot(filtered_array["timestamp"], filtered_array["vectorlen"], 'r', label="filtered data")
    plt.legend()
    ax = plt.gca()
    plt.title(fig_title)
    ax.xaxis.set_major_formatter(date_form)
    plt.gcf().autofmt_xdate()

    return filtered_array


def get_deep_sleep(filtered_data, min_time):
    """
    Calculate all deep sleep phases where no movement happened over a minimum of given length.
    Returns a dict with "deep_start" and "deep_end" arrays.
    """
    min_timedelta = timedelta(minutes=min_time)
    deep_sleep = {"deep_start": [], "deep_end": []}
    current_deep_sleep = False
    begin_deep_sleep = 0
    for i in range(len(filtered_data["vectorlen"])):
        if filtered_data["vectorlen"][i] == 0 and not current_deep_sleep:
            current_deep_sleep = True
            begin_deep_sleep = filtered_data["timestamp"][i]
        elif filtered_data["vectorlen"][i] != 0 and current_deep_sleep:
            current_deep_sleep = False
            if filtered_data["timestamp"][i - 1] - begin_deep_sleep > min_timedelta:
                deep_sleep["deep_start"].append(begin_deep_sleep)
                deep_sleep["deep_end"].append(filtered_data["timestamp"][i - 1])
    return deep_sleep


def plot_sorted_data(vectorlen_data, threshold, title):
    """
    Plot the vector lengths sorted and mark the (in)significant data.
    """
    sorted_array = np.sort(np.array(vectorlen_data["vectorlen"]))
    filtered_array = []
    for i in range(len(sorted_array)):
        if sorted_array[i] < threshold:
            filtered_array.append(sorted_array[i])

    plt.figure(title)
    plt.plot(sorted_array, 'o--', label="significant data")
    plt.plot(filtered_array, 'ro--', label="insignificant data")
    plt.legend()
    plt.title(title)


def set_vectorlen_median(data):
    """
    Calculate the median of the dataset and subtract this median from all vector lengths,
    setting the baseline of the data to y = 0.
    All values will be absolute.
    """
    new_data = {"timestamp": data["timestamp"].copy(), "vectorlen": []}
    numpy_array = np.array(data["vectorlen"])
    median = np.percentile(numpy_array, 50)
    for i in range(len(numpy_array)):
        val = numpy_array[i] - median
        new_data["vectorlen"].append(abs(val))

    name = "Vector lengths with marking at 50%"
    plt.figure(name)
    plt.plot(data["timestamp"], data["vectorlen"], label="vector lengths")
    plt.axhline(median, color='r', label='50% of the data')
    plt.title(name)
    plt.legend(loc="upper left")
    ax = plt.gca()
    ax.xaxis.set_major_formatter(dates.DateFormatter("%H:%M:%S"))
    plt.gcf().autofmt_xdate()

    name = "Vector lengths with new baseline"
    plt.figure(name)
    plt.plot(new_data["timestamp"], new_data["vectorlen"], label="vector lengths")
    plt.title(name)
    plt.legend(loc="upper left")
    ax = plt.gca()
    ax.xaxis.set_major_formatter(dates.DateFormatter("%H:%M:%S"))
    plt.gcf().autofmt_xdate()

    return new_data


def data_to_vectorlen(data):
    vector_data = []
    for i in range(len(data["x"])):
        vector_data.append(calc_vec_length(data["x"][i], data["y"][i], data["z"][i]))
    result = {"timestamp": data["timestamp"].copy(), "vectorlen": vector_data}
    return result


def convert_timestamp(old_timestamps):
    new_time = []
    for time in old_timestamps:
        new_time.append(datetime.fromtimestamp(time / 1000))
    return new_time


def array_as_gradient(arr):
    result = []
    for i in range(len(arr)):
        if i < len(arr) - 1:
            gradient = (arr[i + 1] - arr[i]) / ((i + 1) - i)
            result.append(abs(gradient))
    return result


def calc_vec_length(a, b, c):
    return pow((pow(a, 2) + pow(b, 2) + pow(c, 2)), 0.5)


def get_trend(before, now):
    result = 0
    if before > now:
        result = -1
    elif before < now:
        result = 1
    return result
