import os
import sys
from helper import helper_json
from datetime import timedelta
import xlsxwriter
import numpy


def evaluation(title, sleepCyclePath, evaluationPath):
    sleepCycleJSON = helper_json.convert_dates(helper_json.read(sleepCyclePath))
    evaluationJSON = helper_json.convert_dates(helper_json.read(evaluationPath))

    overlap = {
        "start": [],
        "end": []
    }

    for i in range(len(sleepCycleJSON["start"])):

        for j in range(len(evaluationJSON["start"])):
            start = None

            if sleepCycleJSON["start"][i] <= evaluationJSON["start"][j]:
                if evaluationJSON["start"][j] < sleepCycleJSON["end"][i]:
                    # Überschneidung existiert
                    start = evaluationJSON["start"][j]
            elif evaluationJSON["start"][j] <= sleepCycleJSON["start"][i]:
                if sleepCycleJSON["start"][i] < evaluationJSON["end"][j]:
                    # Überschneidung existiert
                    start = sleepCycleJSON["start"][i]

            if start != None:
                end = evaluationJSON["end"][j]
                if sleepCycleJSON["end"][i] < evaluationJSON["end"][j]:
                    end = sleepCycleJSON["end"][i]
                overlap["start"].append(start)
                overlap["end"].append(end)

    evaluationJSON["tracking_duration"] = evaluationJSON["tracking_end"] - evaluationJSON["tracking_start"]

    comparison = {
        "title": title,
        "evaluation": evaluationJSON,
        "sleep_cycle": sleepCycleJSON,
        "overlap": overlap,
        "deep_sleep_duration_evaluation": sumDuration(evaluationJSON),
        "deep_sleep_duration_sleep_cycle": sumDuration(sleepCycleJSON),
        "deep_sleep_overlap": sumDuration(overlap),
        "comparison_evaluation_sleep_cycle": sumDuration(evaluationJSON) / sumDuration(sleepCycleJSON),
        "comparison_overlap_sleep_cycle": sumDuration(overlap) / sumDuration(sleepCycleJSON)
    }
    return comparison


def sumDuration(data):
    sum = timedelta(0, 0)
    for i in range(len(data["start"])):
        sum += data["end"][i] - data["start"][i]
    return sum


def createExcel(data):
    workbook = xlsxwriter.Workbook('evaluation.xlsx')
    worksheet = workbook.add_worksheet("Nights")

    merge_format_header = workbook.add_format(
        {
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'fg_color': '#a4c2f4'
        }
    )
    cell_format = workbook.add_format(
        {
            'bold': 0,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'bg_color': '#c9daf8'
        }
    )

    evaluation_worksheet = workbook.add_worksheet("Evaluation")
    evaluation_worksheet.write(0, 0, 'Schlaf', cell_format)
    evaluation_worksheet.write(0, 1, 'Comparison Selfmade & Sleep Cycle', cell_format)
    evaluation_worksheet.write(0, 2, 'Comparison Overlap & Sleep Cycle', cell_format)
    evaluation_worksheet.write(0, 3, '', cell_format)
    evaluation_worksheet.write(0, 4, 'Tracking Duration', cell_format)
    evaluation_worksheet.write(0, 5, 'Deep Sleep Duration Selfmade', cell_format)
    evaluation_worksheet.write(0, 6, '', cell_format)
    evaluation_worksheet.write(0, 7, 'Deep Sleep Duration Sleep Cycle', cell_format)
    evaluation_worksheet.write(0, 8, '', cell_format)

    startRow = 0
    for i in range(len(data)):
        # HEADER
        worksheet.merge_range(startRow, 0, startRow, 14, data[i]["title"], merge_format_header)
        worksheet.merge_range(startRow + 1, 0, startRow + 2, 0, 'Phases', cell_format)
        worksheet.merge_range(startRow + 1, 1, startRow + 1, 3, "Selfmade", cell_format)
        worksheet.merge_range(startRow + 1, 4, startRow + 1, 6, "Sleep Cycle", cell_format)
        worksheet.merge_range(startRow + 1, 7, startRow + 1, 9, "Overlap", cell_format)
        worksheet.merge_range(startRow + 1, 10, startRow + 1, 12, "Tracking", cell_format)
        worksheet.merge_range(startRow + 1, 13, startRow + 2, 13, 'Comparison Selfmade & Sleep Cycle', cell_format)
        worksheet.merge_range(startRow + 1, 14, startRow + 2, 14, 'Comparison Overlap & Sleep Cycle', cell_format)

        for j in range(4):
            worksheet.write(startRow + 2, j * 3 + 1, 'Start', cell_format)
            worksheet.write(startRow + 2, j * 3 + 2, 'End', cell_format)
            worksheet.write(startRow + 2, j * 3 + 3, 'Duration', cell_format)

        # Content
        header = 2
        maxRows = max([len(data[i]["evaluation"]["start"]), len(data[i]["sleep_cycle"]["start"]),
                       len(data[i]["overlap"]["start"])])

        for ind, key in enumerate(["evaluation", "sleep_cycle", "overlap"]):
            for j in range(len(data[i][key]["start"])):
                worksheet.write(startRow + header + 1 + j, 0, str(j + 1))
                worksheet.write(startRow + header + 1 + j, ind * 3 + 1,
                                data[i][key]["start"][j].strftime('%d.%m.%Y %H:%M:%S'))
                worksheet.write(startRow + header + 1 + j, ind * 3 + 2,
                                data[i][key]["end"][j].strftime('%d.%m.%Y %H:%M:%S'))
                worksheet.write(startRow + header + 1 + j, ind * 3 + 3,
                                str(data[i][key]["end"][j] - data[i][key]["start"][j]))

        # Boxen
        worksheet.merge_range(startRow + header + 1, 10, startRow + header + maxRows + 1, 10,
                              data[i]["evaluation"]["tracking_start"].strftime('%d.%m.%Y %H:%M:%S'), cell_format)
        worksheet.merge_range(startRow + header + 1, 11, startRow + header + maxRows + 1, 11,
                              data[i]["evaluation"]["tracking_end"].strftime('%d.%m.%Y %H:%M:%S'), cell_format)
        worksheet.merge_range(startRow + header + 1, 12, startRow + header + maxRows + 1, 12,
                              str(data[i]["evaluation"]["tracking_end"] - data[i]["evaluation"]["tracking_start"]),
                              cell_format)

        worksheet.merge_range(startRow + header + 1, 13, startRow + header + maxRows + 1, 13,
                              str(data[i]["comparison_evaluation_sleep_cycle"]), cell_format)
        worksheet.merge_range(startRow + header + 1, 14, startRow + header + maxRows + 1, 14,
                              str(data[i]["comparison_overlap_sleep_cycle"]), cell_format)

        # Footer
        for a in range(10):
            worksheet.write(startRow + header + maxRows + 1, a, '', cell_format)

        worksheet.write(startRow + header + maxRows + 1, 3, str(data[i]["deep_sleep_duration_evaluation"]), cell_format)
        worksheet.write(startRow + header + maxRows + 1, 6, str(data[i]["deep_sleep_duration_sleep_cycle"]),
                        cell_format)
        worksheet.write(startRow + header + maxRows + 1, 9, str(data[i]["deep_sleep_overlap"]), cell_format)

        startRow = startRow + header + maxRows + 3

        # Evaluation
        evaluation_worksheet.write(i + 1, 0, data[i]["title"])
        evaluation_worksheet.write(i + 1, 1, data[i]["comparison_evaluation_sleep_cycle"])
        evaluation_worksheet.write(i + 1, 2, data[i]["comparison_overlap_sleep_cycle"])
        # evaluation_worksheet.write(i+1, 3, data[i]["comparison_overlap_sleep_cycle"]/data[i]["comparison_evaluation_sleep_cycle"])
        evaluation_worksheet.write(i + 1, 3, "=C" + str(i + 2) + "/B" + str(i + 2))
        evaluation_worksheet.write(i + 1, 4,
                                   str(data[i]["evaluation"]["tracking_end"] - data[i]["evaluation"]["tracking_start"]))
        evaluation_worksheet.write(i + 1, 5, str(data[i]["deep_sleep_duration_evaluation"]))
        evaluation_worksheet.write(i + 1, 6, "=F" + str(i + 2) + "/E" + str(i + 2))
        # evaluation_worksheet.write(i+1, 6, str(data[i]["deep_sleep_duration_evaluation"]/(data[i]["evaluation"]["tracking_end"]-data[i]["evaluation"]["tracking_start"])))
        evaluation_worksheet.write(i + 1, 7, str(data[i]["deep_sleep_duration_sleep_cycle"]))
        # evaluation_worksheet.write(i+1, 8, str(data[i]["deep_sleep_duration_sleep_cycle"]/(data[i]["evaluation"]["tracking_end"]-data[i]["evaluation"]["tracking_start"])))
        evaluation_worksheet.write(i + 1, 8, "=H" + str(i + 2) + "/E" + str(i + 2))

    for i in range(9):
        content = ""
        column = chr(65 + i)
        span = "(" + column + "2:" + column + str(len(data) + 1) + ")"

        for j in range(3):
            if i in [1, 2, 3, 6, 8]:
                if j == 0:
                    content = "=AVERAGE" + span
                elif j == 1:
                    content = "=VAR" + span
                elif j == 2:
                    content = "=STDEV" + span

            evaluation_worksheet.write(len(data) + 1 + j, i, content, cell_format)

    evaluation_worksheet.write(len(data) + 1 + 0, 0, "Mittelwert", cell_format)
    evaluation_worksheet.write(len(data) + 1 + 1, 0, "Varianz", cell_format)
    evaluation_worksheet.write(len(data) + 1 + 2, 0, "Standardabweichung", cell_format)
    workbook.close()


def nights_result(data):
    comparison_evaluation_sleep_cycle = numpy.array(list(map(lambda x: x["comparison_evaluation_sleep_cycle"], data)))
    comparison_overlap_sleep_cycle = numpy.array(list(map(lambda x: x["comparison_overlap_sleep_cycle"], data)))
    portion_right_recognised = numpy.array(
        list(map(lambda x: division(x["comparison_overlap_sleep_cycle"], x["comparison_evaluation_sleep_cycle"]), data)))
    portion_deep_sleep_evaluation = numpy.array(list(map(lambda x: division(x["deep_sleep_duration_evaluation"], (
                x["evaluation"]["tracking_end"] - x["evaluation"]["tracking_start"])), data)))
    portion_deep_sleep_sleep_cycle = numpy.array(list(map(lambda x: division(x["deep_sleep_duration_sleep_cycle"], (
                x["evaluation"]["tracking_end"] - x["evaluation"]["tracking_start"])), data)))

    return {
        "comparison_evaluation_sleep_cycle": {
            "mean": numpy.mean(comparison_evaluation_sleep_cycle),
            "var": numpy.var(comparison_evaluation_sleep_cycle),
            "std": numpy.std(comparison_evaluation_sleep_cycle)
        },
        "comparison_overlap_sleep_cycle": {
            "mean": numpy.mean(comparison_overlap_sleep_cycle),
            "var": numpy.var(comparison_overlap_sleep_cycle),
            "std": numpy.std(comparison_overlap_sleep_cycle)
        },
        "portion_right_recognised": {
            "mean": numpy.mean(portion_right_recognised),
            "var": numpy.var(portion_right_recognised),
            "std": numpy.std(portion_right_recognised)
        },
        "portion_deep_sleep_evaluation": {
            "mean": numpy.mean(portion_deep_sleep_evaluation),
            "var": numpy.var(portion_deep_sleep_evaluation),
            "std": numpy.std(portion_deep_sleep_evaluation)
        },
        "portion_deep_sleep_sleep_cycle": {
            "mean": numpy.mean(portion_deep_sleep_sleep_cycle),
            "var": numpy.var(portion_deep_sleep_sleep_cycle),
            "std": numpy.std(portion_deep_sleep_sleep_cycle)
        }
    }


def division(n, d):
    """
    Catch division by zero error and return 0
    """
    return n / d if d else 0


def compare_evaluations():
    data = []
    for path in os.listdir("./nights"):
        path = "nights/" + path
        if os.path.isdir(path) and path[0] != "." and path[0] != "_":
            print("Folder: " + path)
            sleepCyclePath = path + "/sleep-cycle/sleep-cycle.json"
            evaluationPath = path + "/evaluation.json"
            if os.path.isfile(sleepCyclePath) and not os.path.isfile(evaluationPath):
                print("Run evaluation first!")
                sys.exit()
            if os.path.isfile(sleepCyclePath) and os.path.isfile(evaluationPath):
                data.append(evaluation(path, sleepCyclePath, evaluationPath))
    createExcel(data)
    return nights_result(data)
