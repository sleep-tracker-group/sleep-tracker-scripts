from analyzeData import analyzeData_percentile, analyzeData_linreg
from helper import helper_analyze_main, helper_comparison
import numpy
import xlsxwriter

analysis = [
    {"name": "Percentile", "func": analyzeData_percentile.analyze_data, "parameters": {"min_deep_sleep_length": []}},
    {"name": "LinReg", "func": analyzeData_linreg.analyze_data,
     "parameters": {"min_deep_sleep_length": [], "steps": [], "angle_threshold": []}}
]

options = "SELECT ANALYSIS FOR ALL NIGHTS\n"
for i in range(len(analysis)):
    options += "[" + str(i) + "] - " + analysis[i]["name"] + "\n"

print(options)
input_var = int(input("Enter option: "))
print("You selected: " + analysis[input_var]["name"])

print("##################################\nSET PARAMETERS\n")
for key in analysis[input_var]["parameters"].keys():
    print("---")
    print(key)
    key_from = float(input("From: "))
    key_to = float(input("To: "))
    key_step = float(input("Step: "))
    # print(list(range(key_from,key_to,key_step)))
    analysis[input_var]["parameters"][key] = list(numpy.arange(key_from, key_to, key_step))
    # print(analysis[input_var]["parameters"][key])
print("##################################")

first_key = list(analysis[input_var]["parameters"].keys())[0]
parameter_configs = []

for x in analysis[input_var]["parameters"][first_key]:
    parameter_configs.append({first_key: x})

if len(list(analysis[input_var]["parameters"].keys())) > 1:
    for key in analysis[input_var]["parameters"].keys():
        if key != first_key:
            combined = []
            for i in range(len(parameter_configs)):
                for val in analysis[input_var]["parameters"][key]:
                    combined.append({**parameter_configs[i], **{key: val}})
            parameter_configs = combined
print(len(parameter_configs))

results = []

config_counter = 1
for config in parameter_configs:
    print("##################################")
    print("config " + str(config_counter) + " of " + str(len(parameter_configs)))
    print("run analyze with parameters: " + str(config))
    helper_analyze_main.analyze_all_nights(analysis[input_var]["func"], config)

    print("run comparison")
    results.append({
        "parameters": config,
        "result": helper_comparison.compare_evaluations()
    })

    config_counter += 1

print("##################################")
print("##################################")
print("done")
print(results)


def createExcel(data):
    workbook = xlsxwriter.Workbook('config_comparison.xlsx')

    merge_format_header = workbook.add_format(
        {
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'fg_color': '#a4c2f4'
        }
    )
    cell_format = workbook.add_format(
        {
            'bold': 0,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'bg_color': '#c9daf8'
        }
    )

    worksheet = workbook.add_worksheet("Configs")

    column = 0
    for param in data[0]["parameters"].keys():
        # worksheet.write(0, column, param, cell_format)
        worksheet.merge_range(0, column, 1, column, param, cell_format)
        column += 1

    for metric in data[0]["result"].keys():
        worksheet.merge_range(0, column, 0, column + len(data[0]["result"][metric]) - 1, metric, cell_format)
        for x in data[0]["result"][metric]:
            worksheet.write(1, column, x, cell_format)
            column += 1

    row = 2
    for config in data:
        column = 0
        for param in config["parameters"].keys():
            worksheet.write(row, column, config["parameters"][param], cell_format)
            column += 1

        for metric in config["result"].keys():
            for sub in config["result"][metric].keys():
                worksheet.write(row, column, config["result"][metric][sub])
                column += 1
        row += 1

    workbook.close()


createExcel(results)
